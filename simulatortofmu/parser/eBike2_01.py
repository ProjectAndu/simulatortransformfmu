# -*- coding: utf-8 -*-
"""
Created on Thu Nov  2 17:59:54 2017

@author: Huw Williams 
"""

import numpy as np # not used but needed when arrays are required
import matplotlib.pyplot as plt
import pandas as pd # not used but pandas may be needed for dealing with data frames that are output
from huwsim import huwsim, objectview # import huwsim class module that runs simulation models
from IPython.display import display

def mkCeB():
    # Enter model constants here, one per line. Don't use integer values eg m = 1.0 NOT m = 1
    tstart = 0. # Simulation start time [s]
    tstop = 20. # Simulation stop time [s]
    fsample = 10. # Simulation reporting sample frequency [Hz]
    x0 = 0. # Initial value of x
    v0 = 3. # Initial value of v
    xfb0 = 1. # Initial value of xfb
    m = 80.0 # kg mass of rider and eBike
    g = 9.81 # m/s^2 gravity
    Crr = 0.005 # - rolling resistance coefficient
    rho = 1.225 # kg/m^3 air density
    Cd = 0.8 # - drag coefficient
    A = 0.425 # m^2 frontal area
    Kcc = 100. # Ns/m cruise control proportional feedback gain
    Tcc = 5. # s cruise control integral term time constant
    vtarg = 4. # m/s cruise control set speed
    PeM =  250. # W electric motor power
    Prid = 100. # W human power (assumed constant for now)
    Prear = 0.6 # - proportion of weight on rear axle
    mu = 0.1 # - coefficient of friction
    # Return dictionary containing constant names and values.
    return locals()

CeB = mkCeB() # Build constants dictionary from code

# This dictionary contains the state variable names and associates them to the initial condition names
S2IeB = {'x': 'x0', 'v': 'v0', 'xfb': 'xfb0'}

def mkEeB(S, t, Cd):
    D ={} # Create empty dictionary for returning derivatives
    C = objectview(Cd) # Create object from dictionary for better syntax
    
    # Model equations are here - this code can be modified to create new models
    x = S['x'] # Unpacking the state variables x and v from their dictionary
    v = S['v'] # creates algebraic variables that are available for output
    xfb = S['xfb'] # integral control state
    Frid = C.Prid / v
    xfbdot = C.vtarg - v
    Fccdem = C.Kcc * (xfbdot + 1./C.Tcc * xfb)
    Pdem = v * Fccdem
    if Pdem < C.PeM:
        Fcc = Fccdem
    else:
        Fcc = C.PeM / v
    Pact = v * Fcc
    Frr = C.m * C.g * C.Crr
    Fd = 0.5 * C.rho * C.Cd * C.A * v**2
    Fr = Frr + Fd
    Pr = v * Fr
    Ftracdem = Frid + Fcc
    Ftrac = min(C.mu * C.m * C.g * C.Prear, Ftracdem)
    slip = C.mu * C.m * C.g * C.Prear < Ftracdem # Logical that flags slip condition
    F = Ftrac - Fr
    xdot = v
    vdot = F/C.m
    D['x'] = xdot # These keys must match the ones for the corresponding
    D['v'] = vdot # states defined in mkI()
    D['xfb'] = xfbdot
    vkph = v * 3.6
    
    # The following code should not be modified
    A = locals()
    # Delete dictionaries from A to just leave the algebraic variables
    del A['S']
    del A['Cd']
    del A['C']
    del A['D']
    del A['t']
    return D, A

def sendSim():
    sim = huwsim(CeB, S2IeB, mkEeB)
    return sim


# Call to huwsim class module where all the heavy lifting is done
sim = huwsim(CeB, S2IeB, mkEeB)
# Plot the output
sim.df[['F', 'Ftrac', 'Ftracdem', 'Frid', 'Fcc', 'Fr']].plot()
plt.grid()
plt.title('eBike Closed-Loop Simulation')
sim.df[['vkph']].plot()
plt.grid()
plt.title('eBike Closed-Loop Simulation')
sim.df[['Pact', 'Pdem', 'Pr']].plot()
plt.grid()
plt.title('eBike Closed-Loop Simulation')
sim.df[['slip']].plot()
plt.grid()
plt.title('eBike Closed-Loop Simulation')
# Display the constant values
display(CeB)