import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt
import pandas as pd

class huwsim:
    """
    The huwsim class encapsulates Huw's simulation model code into a class module to make it easier to use
    """
    
    def __init__(self, Constants, States2ICs, MakeEquations):
        try:
            df, mrow = Constants # Tuple containing pandas data frame and row number
            self.C = self._mkC(df, mrow)
        except:
            self.C = Constants # Dictionary of constant definitions
        self.S2I = States2ICs # Dictionary associating states to initial condition constants
        self.I = self._mkI() # Dictionary of initial states
        self.E = MakeEquations # Function that contains simulation model equations
        self.t0 = self.C['tstart']
        self.t1 = self.C['tstop']
        self.h = 1. / self.C['fsample']
        self.t = np.arange(self.t0, self.t1, self.h)
        self.X0 = self._dict2array(self.I)
        self.f, self.g = self._mkfg(MakeEquations)
        self.X = odeint(self.f, self.X0, self.t, args=(self.C, self.I)) # Call to odeint that returns state variables
        self.Y, self.A = self._mkYA()
        self.df = self._mkdf()
    
    def _mkC(self, df, mrow):
        out = {}
        for col in df:
            val = df[col].values[mrow]
            try:
                out[col] = eval(val) # Evaluates a string then stores it as an array 
            except ValueError:
                out[col] = float(val) # If entry is not evaluatable it is a scalar so cast to float and store
        return out
    
    def _mkI(self):
        d = {}
        for key in sorted(self.S2I):
            d[key] = self.C[self.S2I[key]]
        return d

    def _dict2array(self, d): # Define function that converts dictionaries to 1D numpy arrays
        n = len(d)
        out = np.zeros((n,))
        i = 0
        for key in sorted(d): # keys are sorted to ensure array elements are always in the same order
            out[i] = d[key]
            i = i + 1
        return out
    
    def _array2dict(self, a, d):
        out = {}
        i = 0
        for key in sorted(d): # The input dictionary needs to have the right keys - usually the initial conditions dictionary
            out[key] = a[i]
            i = i + 1
        return out
    
    def _mkfg(self, fin):
        def fout(X, t, C, I):
            S = self._array2dict(X, I) # Create dictionary of state variables
            D, A = self.E(S, t, C) # Get dictionaries of derivatives and algebraic variables from user code
            return self._dict2array(D) # Return array of derivatives
        def gout(X, t, C, I):
            S = self._array2dict(X, I) # Create dictionary of state variables
            D, A = self.E(S, t, C) # Get dictionaries of derivatives and algebraic variables from user code
            return self._dict2array(A) # Return array of algebraic variables
        return fout, gout # This returns function objects that can be used by the library function odeint and the class module
    
    def _mkYA(self):
        D, A = self.E(self.I, self.t0, self.C) # Get initial algebraic variables dictionary, A
        m = len(self.t)
        n = len(A)
        Y = np.zeros((m, n)) # Initialise Y
        for i in range(m):
            Y[i, :] = self.g(self.X[i, :], self.t[i], self.C, self.I) # Fill Y row by row
        return Y, A
    
    def _mkdf(self):
        df = pd.DataFrame(index=self.t)
        df.index.name = 't'
        i = 0
        for key in sorted(self.A):
            df[key] = self.Y[:, i]
            i = i + 1
        return df

class huwdoe:
    """
    This class runs a doe from an Excel file
    """
    
    def __init__(self, InFile, States2ICs, MakeEquations):
        self.Cdf = pd.read_excel(InFile)
        self.S2I = States2ICs # Dictionary associating states to initial condition constants
        self.E = MakeEquations # Function that contains simulation model equations
        self.Yl = self._mkYl()
    
    def _mkYl(self):
        mrow = 0
        out = []
        for row in self.Cdf.itertuples():
            sim = huwsim((self.Cdf, mrow), self.S2I, self.E)
            out.append(sim.df)
            mrow = mrow + 1
        return out

class objectview(object):
    """
    This class creates an object from a dictionary that allows nicer syntax
    
    e.g.
    
    Cd = {m:1.0, k:6.3}
    C = objectview(Cd)
    print(C.m, C.k)
    1.0, 6.3
    """
    def __init__(self, d):
        self.__dict__ = d